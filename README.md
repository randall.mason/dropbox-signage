# dropbox-signage

This is all the configuration for Dropbox-Signage

Requirements:
 * [Homebrew](https://brew.sh)
 * Ansible
 * sshpass

This uses ansible extensively.  `brew install ansible sshpass` or `apt install ansible sshpass` before you start.

To create a dropox signage pi:
* Download raspbian
* Use Etcher or the Chromebook Recovery Tool to write it to an SD Card.
* Get this repository and all submodules: `git clone --recurse-submodules https://gitlab.com/randall.mason/dropbox-signage.git`
* Create a file named `ssh` on the `boot` volume of the raspberrypi
  * There is an empty file in this directory for easy copy-pasta!
* Create a wifi settings file (`wpa_supplicant.conf`) in the same place based off of the file in this directory if you don't have a wired network.
  * Enter your Country Code (Like `us`)
  * Enter your SSID (Network Name)
  * Enter your PSK (The password for your network)
* Eject that drive and put it in a raspberrypi hooked to the network (either wired or, if you edited the `wpa_supplicant.conf`, wireless)
* wait for it to boot up (just wait a minute or two, or, plug it into a monitor and wait for it to print `raspberrypi login:` on the screen)
* Edit the variables in `group_vars/all` or set them from the command line:
* run `ansible-playbook site.yaml -l "bootstrap" -vv -e "wifi_ssid=ChurchyMcChurchFace wifi_psk='Bla Bla Bla' sign_name='Randall 3'"`
   - You MUST set `sign_name` to something unique
* Then, when it reboots, add that name to your inventory.  Add a new host under `hosts:` in a named section, like `dev` but which explains where or the type of location you're hitting.

