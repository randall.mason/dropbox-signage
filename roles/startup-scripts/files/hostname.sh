#!/bin/bash -e

hostname=$(basename "$(cat /boot/signage.txt)" | sed -e 's/[- ]//g')
echo ${hostname,,}
grep ${hostname,,} /etc/hostname || {
sudo augtool set /files/etc/hostname/hostname ${hostname,,}
sudo augtool set /files/etc/hosts/5/canonical ${hostname,,}

sudo fatlabel /dev/mmcblk0p1 ${hostname,,}

hostname -F /etc/hostname

sudo reboot
}
